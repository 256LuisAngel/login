<?php
$app->get('/','FrontEnd::indexAction')->bind('raiz');
$app->get('/registro','FrontEnd::registro')->bind('portada');
$app->post('/acceso','FrontEnd::clienteRegistrado')->bind('Cliente registrado');
$app->get('/productos','FrontEnd::catalogoProductos')->bind('Catalogo productos');
$app->get('/login','FrontEnd::login')->bind('Inicio de sesion');
$app->post('/login','FrontEnd::loginValidacion')->bind('Sesion Iniciada');
